﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uber.Models
{
    public partial class Clientes
    {
        public override string ToString()
        {
            return $"{Id}) {Nombre} {Apellido}";
        }
    }
}
