﻿using System;
using System.Collections.Generic;

namespace Uber.Models
{
    public partial class Autos
    {
        public int Id { get; set; }
        public string Modelo { get; set; }
        public string Marca { get; set; }
        public string Matricula { get; set; }
        public string Poliza { get; set; }
        public int ConductorId { get; set; }

        public virtual Conductores Conductor { get; set; }
    }
}
