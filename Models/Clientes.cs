﻿using System;
using System.Collections.Generic;

namespace Uber.Models
{
    public partial class Clientes
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
    }
}
