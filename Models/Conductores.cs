﻿using System;
using System.Collections.Generic;

namespace Uber.Models
{
    public partial class Conductores
    {
        public Conductores()
        {
            Autos = new HashSet<Autos>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Licencia { get; set; }
        public string Genero { get; set; }

        public virtual ICollection<Autos> Autos { get; set; }
    }
}
