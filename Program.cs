﻿using System;
using Uber.Models;

namespace Uber
{
    class Program
    {
        static void Main(string[] args)
        {
            //CrearCliente();
            ListarClientes();
        }

        public static void CrearCliente()
        {
            Console.WriteLine("Nuevo cliente");
            Clientes cliente = new Clientes();

            Console.Write("Nombre: ");
            cliente.Nombre = Console.ReadLine();

            Console.Write("Apellido: ");
            cliente.Apellido = Console.ReadLine();

            Console.Write("Teléfono: ");
            cliente.Telefono = Console.ReadLine();

            using(UberContext context = new UberContext())
            {
                context.Add(cliente);
                context.SaveChanges();
                Console.WriteLine("Cliente guardado");
            }
        }

        public static void ListarClientes()
        {
            Console.WriteLine("Lista de clientes");
            using(UberContext context = new UberContext())
            {
                foreach(Clientes cliente in context.Clientes)
                {
                    Console.WriteLine(cliente);
                }
            }
        }
    }
}
